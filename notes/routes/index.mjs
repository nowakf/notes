import express from 'express';
import * as notes from '../models/notes';

export const router = express.Router();

/* GET home page. */
router.get('/', async (req, res, next) => {
	try {
		const notelist = await getKeyTitlesList();
		res.render('index', {
			title: 'Notes',
			notelist: notelist,
			user: req.user ? req.user : undefined
		});
	} catch (e) {
		next(e);
	}
});

export function socketio(io) {
  let home = io.of('/home');
  let emitNoteTitles = async() => {
    const notelist = await getKeyTitlesList();
    home.emit('notetitles', { notelist });
  }
  notes.events.on('notecreated', emitNoteTitles); 
  notes.events.on('noteupdate', emitNoteTitles); 
  notes.events.on('notedestroy', emitNoteTitles);
}

async function getKeyTitlesList() {
	const keylist = await notes.keylist();
	let keyPromises = keylist.map((key) => {
		return notes.read(key).then((note) => {
			return {
				key: note.key,
				title: note.title
			};
		});
	});
	return Promise.all(keyPromises);
}
