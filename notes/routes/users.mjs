import path from 'path';
import util from 'util';
import express from 'express';
import passport from 'passport';
import passportLocal from 'passport-local';
import passportTwitter from 'passport-twitter';
const LocalStrategy = passportLocal.Strategy;
const TwitterStrategy = passportTwitter.Strategy;
import * as usersModel from '../models/users-superagent';
import { sessionCookieName } from '../app';

export const router = express.Router();

import DBG from 'debug';
const debug = DBG('notes:router-users');
const error = DBG('notes:error-users');

export function initPassport(app) {
	app.use(passport.initialize());
	app.use(passport.session());
}

export function ensureAuthenticated(req, res, next) {
	try {
		if (req.user) next();
		else res.redirect('/users/login');
	} catch (e) {
		next(e);
	}
}

router.get('/login', (req, res, next) => {
	try {
		res.render('login', { title: 'Login to Notes', user: req.user });
	} catch (e) {
		next(e);
	}
});

router.post(
	'/login',
	passport.authenticate('local', {
		successRedirect: '/',
		failureRedirect: 'login'
	})
);

router.get('/auth/twitter', passport.authenticate('twitter'));

router.get(
	'/auth/twitter/callback',
	passport.authenticate('twitter', {
		successRedirect: '/',
		failureRedirect: '/users/login'
	})
);

router.get('/logout', (req, res, next) => {
	try {
		req.session.destroy();
		req.logout();
		res.clearCookie(sessionCookieName);
		res.redirect('/');
	} catch (e) {
		next(e);
	}
});

passport.use(
	new LocalStrategy(async (username, password, done) => {
		try {
			let check = await usersModel.userPasswordCheck(username, password);
			debug(check);
			if (check.check) {
				done(null, {
					id: check.username,
					username: check.username
				});
			} else {
				done(null, false, check.message);
			}
		} catch (e) {
			done(e);
		}
	})
);

const twittercallback = process.env.TWITTER_CALLBACK_HOST ? process.env.TWITTER_CALLBACK_HOST : 'http://localhost:3000';

passport.use(
	new TwitterStrategy(
		{
			consumerKey: process.env.TWITTER_CONSUMER_KEY,
			consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
			callbackURL: `${twittercallback}/users/auth/twitter/callback`
		},
		async function(token, tokenSecret, profile, done) {
			try {
				done(
					null,
					await usersModel.findOrCreate({
						id: profile.username,
						username: profile.username,
						password: '',
						provider: profile.provider,
						familyName: profile.displayName,
						givenName: '',
						middleName: '',
						photos: profile.photos,
						emails: profile.emails
					})
				);
			} catch (e) {
				next(e);
			}
		}
	)
);

passport.serializeUser(function(user, done) {
	try {
		done(null, user.username);
	} catch (e) {
		done(e);
	}
});

passport.deserializeUser(async (username, done) => {
	try {
		let user = await usersModel.find(username);
		done(null, user);
	} catch (e) {
		done(e);
	}
});
