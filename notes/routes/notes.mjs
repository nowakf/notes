import express from 'express';
import * as notes from '../models/notes';
import { ensureAuthenticated } from './users';
import * as messages from '../models/messages-sequelize';
import DBG from 'debug';
const debug = DBG('notes:model-notes');
const error = DBG('notes:error-notes');

export const router = express.Router();

// Add note
router.get('/add', ensureAuthenticated, (req, res, next) => {
  try {
    res.render('noteedit', {
      title: 'Add a note',
      docreate: true,
      notekey: '',
      note: undefined,
      user: req.user
    });
  } catch(e) {
    next(e);
  }

});

router.get('/edit', ensureAuthenticated, async (req, res, next) => {
  try {
    let note = await notes.read(req.query.key);
    res.render('noteedit', {
      title: note ? `Edit ${note.title}` : 'Add a Note',
      docreate: false,
      notekey: req.query.key,
      note: note,
      user: req.user
    })
  } catch(e) {
    next(e);
  }

});

router.post('/save', ensureAuthenticated, (req, res, next) => {
  let note;
  if (req.body.docreate === 'create') {
    note = notes.create(req.body.notekey, req.body.title, req.body.body);
  } else {
    note = notes.update(req.body.notekey, req.body.title, req.body.body);
  }
  res.redirect(`/notes/view?key=${req.body.notekey}`);
});

router.get('/destroy', ensureAuthenticated, async (req, res, next) => {
  try {
    let note = await notes.read(req.query.key);
    res.render('notedestroy', {
      title: note ? `Delete ${note.title}` : '',
      notekey: req.query.key,
      note: note,
      user: req.user
    })
  } catch(e) {
    next(e);
  }
});

router.post('/destroy/confirm', ensureAuthenticated, (req, res, next) => {
  notes.destroy(req.body.notekey);
  res.redirect('/');
});

router.get('/view', async (req, res, next) => {
  try {
    let note = await notes.read(req.query.key);
    res.render('noteview', {
      title: note ? note.title : '',
      notekey: req.query.key,
      note: note,
      user: req.user ? req.user : undefined
    })
  } catch(e) {
    next(e);
  }
});

router.post('/make-comment', ensureAuthenticated, async (req, res, next) => {
  try {
    await messages.postMessage(req.body.from, req.body.namespace, req.body.message);
    res.status(200).json({});
  } catch(e) {
    res.status(500).end(e.stack);
  }
});

router.post('/del-message', ensureAuthenticated, async (req, res, next) => {
  try {
    await messages.destroyMessage(req.body.id, req.body.namespace);
    res.status(200).json({});
  } catch(e) {
    res.status(200).end(e.stack);
  }
});

export function socketio(io) {
  io.of('/view').on('connection', function(socket) {
    // 'cb' is a function sent from the browser, to which we
    // send the messages for the named note.
    socket.on('getnotemessages', (namespace, cb) => {
      messages.recentMessages(namespace).then(cb)
      .catch(err => console.error(err.stack));
    });
  });

  notes.events.on('noteupdated', newnote => {
    io.of('/view').emit('noteupdated', newnote);
  });
  notes.events.on('notedestroy', data => {
    io.of('/view').emit('notedestroy', data);
  });
  messages.emitter.on('newmessage', newmsg => {
    io.of('/view').emit('newmessage', newmsg); 
  });
  messages.emitter.on('destroymessage', data => {
    io.of('/view').emit('destroymessage', data); 
  });
};