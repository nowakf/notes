import EventEmitter from 'events';

class NotesEmitter extends EventEmitter {
  noteCreated(note) {
    this.emit('notecreated', note);
  }
  noteUpdated(note) {
    this.emit('noteupdated', note)
  }
  noteDestroy(data) {
    this.emit('notedestroy', data);
  }
}

export default new NotesEmitter();